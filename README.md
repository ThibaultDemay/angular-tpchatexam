# angular-tpChatExam

Sujet : Projet Angular visant à développer un chat à plusieurs channels avec formulaire d'identification.

Le projet permet de se connecter (pseudo enregistré en SessionStorage) et d'afficher l'intégralité des messages stockés dans le db.json.
On peut rentrer un message, le valider, et il est inscrit en base avec notre pseudo.
Reste à faire à la fin du temps imparti :
    La date et l'heure du message n'ont pas été implémentées.
    La liste des channels s'affiche dans le panneau de gauche mais n'est pas clickable.

Bonne correction,

Thibault Demay