import { Component, OnInit, Input } from '@angular/core';
import { Channel } from "../channel";

@Component({
  selector: 'app-channel',
  templateUrl: './channel.component.html',
  styleUrls: ['./channel.component.scss']
})
export class ChannelComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  @Input()
  channel: Channel;

}
