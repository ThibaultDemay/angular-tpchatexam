import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormsModule, NgForm, FormControl, FormArray } from '@angular/forms';

@Component({
  selector: 'app-formlogin',
  templateUrl: './formlogin.component.html',
  styleUrls: ['./formlogin.component.scss']
})
export class FormloginComponent implements OnInit {
  model: FormGroup;
  public isSubmit: boolean = false;
  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.model = this.formBuilder.group({
      pseudo: ''
    })
  }

  get pseudo() {
    return this.model.get('pseudo');
  }

  savePseudoInSessionStorage(){
    sessionStorage.setItem('pseudo', this.model.value.pseudo);
  }

  // La méthode appelée quand on clique sur le bouton du formulaire de pseudo
  sendForm(): void {
    this.savePseudoInSessionStorage();
    this.isSubmit = true; // Pour changer d'affichage et afficher les channels
  }



}
