import { Component, OnInit } from '@angular/core';
import { Channel } from "../channel";
import { MessageService } from '../message.service';

@Component({
  selector: 'app-list-channel',
  templateUrl: './list-channel.component.html',
  styleUrls: ['./list-channel.component.scss']
})
export class ListChannelComponent implements OnInit {

 

  constructor(private messageService: MessageService) { }

  ngOnInit() {
    this.refreshChannels();
  }

  refreshChannels(){
      //appel au service pour récupérer les messages via subscribe et les stocker dans l'attribut this.messages du component
      this.messageService.getChannels().subscribe(
        (channels: Channel[]) => this.channels = channels
      )
  }

}
