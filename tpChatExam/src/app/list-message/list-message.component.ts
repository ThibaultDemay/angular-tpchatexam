import { Component, OnInit } from "@angular/core";
import { Message } from "../message";
import { MessageService } from '../message.service';

@Component({
  selector: 'app-list-message',
  templateUrl: './list-message.component.html',
  styleUrls: ['./list-message.component.scss']
})
export class ListMessageComponent implements OnInit {
  messages : Message[] = [];

  newMessageContent: string;

  constructor(private messageService: MessageService) { }

  ngOnInit() {
    this.refreshMessages();
  }

  private refreshMessages() {
    //appel au service pour récupérer les messages via subscribe et les stocker dans l'attribut this.messages du component
    this.messageService.getMessages().subscribe(
      (messages: Message[]) => this.messages = messages
    )
  }

  addMessage() {
      const newMessage: Message = {
        pseudo: sessionStorage.getItem('pseudo'), // on récupère le pseudo stocké dans le session storage
        content: this.newMessageContent,
        idChannel: 1, // A modifier
      }
      this.messageService.addMessage(newMessage).subscribe((message: Message) => {
        this.messages.push(message)
    })
  }

  updateTodo(message : Message) {
      this.messageService.updateMessage(message).subscribe()
    }

}


