//AKA ChatService :)

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import {Message} from './message';
import {Channel} from './channel';


@Injectable({
  providedIn: 'root'
})
export class MessageService {

  constructor(private http: HttpClient) { }

getMessages() : Observable<Message[]> {
  return this.http.get<Message[]>(`${environment.backUrl}/messages`)
}

addMessage(message: Message): Observable<Message> {
  return this.http.post<Message>(`${environment.backUrl}/messages`, message)
}

deleteMessage(message: Message): Observable<void> {
  return this.http.delete<void>(`${environment.backUrl}/messages/${message.id}`)
}

updateMessage(message: Message): Observable<void> {
  return this.http.put<void>(`${environment.backUrl}/messages/${message.id}`, message)
}

getChannels() : Observable<Channel[]>{
  return this.http.get<Channel[]>(`${environment.backUrl}/channels`)
}


}